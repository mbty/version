# Version
This library provides a well-integrated `version` type for C++.

It was made with a limited form of [semantic versioning](https://semver.org/)
in mind, and works consequently with versions of the form `major.minor.patch`.

## Constructors
```C++
version(unsigned int major, unsigned int minor, unsigned int patch) noexcept;
version(const unsigned int (&list)[3]) noexcept;
```
The second constructor allows writing versions as `{major, minor, patch}`.

## Access
The members of the version class are all public.
```C++
unsigned int major;
unsigned int minor;
unsigned int patch;
```

## Comparison
All six standard comparison functions are defined (`<`, `<=`, `>`, `>=`, `==`,
`!=`).  They are noexcept and operate on const references.

## Helper functions
```C++
bool are_same_up_to_minor(const version &v1, const version &v2) noexcept;
bool are_same_up_to_patch(const version &v1, const version &v2) noexcept;
```

## Legal
This library is free software. It comes without any warranty, to the extent
permitted by applicable law. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2, included
in the LICENSE.md file.
