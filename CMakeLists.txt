cmake_minimum_required(VERSION 3.10)

project(version VERSION 1.0.0 DESCRIPTION "Version class" LANGUAGES CXX)
set(CMAKE_CXX_STANDARD 17)

add_subdirectory(src)
add_subdirectory(example)
