// This file is part of the version library - gitlab.com/mbty/version
#include "version.hh"
namespace version {
  version::version(
    unsigned int major, unsigned int minor, unsigned int patch
  ) noexcept
  : major(major), minor(minor), patch(patch)
  { }

  version::version(const unsigned int (&list)[3]) noexcept
  : major(list[0]), minor(list[1]), patch(list[2])
  { }

  // # Comparison operators
  bool operator<(const version &v1, const version &v2) noexcept {
    return
      (v1.major < v2.major)
      || ((v1.major == v2.major) && (v1.minor < v2.minor))
      || ((v1.major == v2.major) && (v1.minor == v2.minor)
         && (v1.patch < v2.patch));
  }

  bool operator<=(const version &v1, const version &v2) noexcept {
    return !(v1 > v2);
  }

  bool operator>(const version &v1, const version &v2) noexcept {
    return
      (v1.major > v2.major)
      || ((v1.major == v2.major) && (v1.minor > v2.minor))
      || ((v1.major == v2.major) && (v1.minor == v2.minor)
         && (v1.patch > v2.patch));
  }

  bool operator>=(const version &v1, const version &v2) noexcept {
    return !(v1 < v2);
  }

  bool operator==(const version &v1, const version &v2) noexcept {
    return
      (v1.major == v2.major)
      && (v1.minor == v2.minor)
      && (v1.patch == v2.patch);
}

  bool operator!=(const version &v1, const version &v2) noexcept {
    return !(v1 == v2);
  }

  // # Helpers
  bool are_same_up_to_minor(const version &v1, const version &v2) noexcept {
    return (v1.major == v2.major);
  }

  bool are_same_up_to_patch(const version &v1, const version &v2) noexcept {
    return (v1.major == v2.major && v1.minor == v2.minor);
  }

  // # IO
  std::ostream &operator<<(std::ostream &os, const version &version) {
    // Buffering ensures sequential display
    std::ostringstream buffer;
    buffer << version.major << '.' << version.minor << '.' << version.patch;
    os << buffer.str();
    return os;
  }
}
