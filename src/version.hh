// This file is part of the version library - gitlab.com/mbty/version
#pragma once
#include <algorithm>
#include <ostream>
#include <sstream>
#include <string>
#include <vector>
// For compatibility reasons with the GNU C library:
// "In the GNU C Library, "major" is defined
// by <sys/sysmacros.h>. For historical compatibility, it is
// currently defined by <sys/types.h> as well, but we plan to
// remove this soon. To use "major", include <sys/sysmacros.h>
// directly. If you did not intend to use a system-defined macro
// "major", you should undefine it after including <sys/types.h>."
#undef major
#undef minor
namespace version {
  class version {
    public: // Functions
      version(unsigned int major, unsigned int minor, unsigned int patch) noexcept;
      version(const unsigned int (&list)[3]) noexcept;

    public: // Members
      unsigned int major;
      unsigned int minor;
      unsigned int patch;
  };

  // # Comparison operators
  bool operator< (const version &v1, const version &v2) noexcept;
  bool operator<=(const version &v1, const version &v2) noexcept;
  bool operator> (const version &v1, const version &v2) noexcept;
  bool operator>=(const version &v1, const version &v2) noexcept;
  bool operator==(const version &v1, const version &v2) noexcept;
  bool operator!=(const version &v1, const version &v2) noexcept;

  // # Other helpers
  bool are_same_up_to_minor(const version &v1, const version &v2) noexcept;
  bool are_same_up_to_patch(const version &v1, const version &v2) noexcept;

  // # IO
  std::ostream &operator<<(std::ostream &os, const class version &version);
}
