// This file is part of the version library - gitlab.com/mbty/version
#include <iostream>
#include "version.hh"
void test(const version::version &v1, const version::version &v2) {
  std::cout << v1 << " <  " << v2 << " " << (v1 < v2)  << "\n";
  std::cout << v1 << " <= " << v2 << " " << (v1 <= v2) << "\n";
  std::cout << v1 << " >  " << v2 << " " << (v1 > v2)  << "\n";
  std::cout << v1 << " >= " << v2 << " " << (v1 >= v2) << "\n";
  std::cout << v1 << " == " << v2 << " " << (v1 == v2) << "\n";
  std::cout << v1 << " != " << v2 << " " << (v1 != v2) << "\n";
}

int main() {
  std::cout << std::boolalpha;

  test({1, 2, 3}, {12, 2, 3});
  test({1, 2, 3}, {1, 2, 3});
  test({12, 2, 3}, {2, 2, 3});
  test({1, 2, 4}, {1, 2, 3});
}
