add_executable(version_example example.cc)
set_target_properties(version_example PROPERTIES EXCLUDE_FROM_ALL TRUE)
target_include_directories(version_example PRIVATE ../src/)
target_link_libraries(version_example PRIVATE version)
